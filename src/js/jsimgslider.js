// Fix click on sliderPagination
const imgSlider = document.querySelector('#img-slider');
const jumpClk = document.querySelector('.slide-img');
const leftClk = document.querySelector('#icon-left');
const rightClk = document.querySelector('#icon-right');

let lastImg = 0;
let currentImg = 0;
let totalSlides = 0;
const slides = imgSlider.getElementsByClassName("img-slide");
const buttons = imgSlider.getElementsByClassName("img-btn");

// Load all event listeners
LoadEventListeners();

// Create event listeners
function LoadEventListeners() {
  // DOM load tasks
  document.addEventListener('DOMContentLoaded', initSlider);
  jumpClk.addEventListener('click', jumpImg);
  leftClk.addEventListener('click', leftImg);
  rightClk.addEventListener('click', rightImg);
}

function initSlider() {
  // Get the total number of figures
  totalSlides = parseInt(slides.length);
  //console.log('Total slides = ' + totalSlides);
  // Shift the first slide to make it visible
  slides[currentImg].style.left = "0";
  buttons[currentImg].className = "img-btn img-btn-clk";
}

function rightImg() {
  // Obtain the number of the requested image
  currentImg += 1;
  if (currentImg >= totalSlides) {
    currentImg = 0;
  }
  // console.log(lastImg, currentImg);
  // Update the style of images
  slides[currentImg].style.left = "0";
  slides[lastImg].style = '';
  buttons[currentImg].className = "img-btn img-btn-clk";
  buttons[lastImg].className = "img-btn";
  lastImg = currentImg;
}

function leftImg() {
  // Obtain the number of the requested image
  currentImg -= 1;
  if (currentImg < 0) {
    currentImg = totalSlides-1;
  }
  // console.log(lastImg, currentImg);
  // Update the style of images
  slides[currentImg].style.left = "0";
  slides[lastImg].style = '';
  buttons[currentImg].className = "img-btn img-btn-clk";
  buttons[lastImg].className = "img-btn";
  lastImg = currentImg;
}

function jumpImg(e) {
  //console.log(e.target.parentElement.classList.contains('slide-img'));
  // console.log(e.target.textContent);
  // e.target.style.background = "#fff";
  if (e.target.parentElement.classList.contains('slide-img')) {
    currentImg = parseInt(e.target.textContent) - 1;
    // Obtain the number of the requested image
    // Update the style of images
    slides[currentImg].style.left = "0";
    buttons[currentImg].className = "img-btn img-btn-clk";
    if (lastImg != currentImg) {
      slides[lastImg].style = '';
      buttons[lastImg].className = "img-btn";
    }
  }
  // Cancel the default action
  e.preventDefault();
  lastImg = currentImg;
}
